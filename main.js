import fs from 'fs'
import util from 'util'

import Parser from 'node-dbf';
import * as dsv from 'd3-dsv';

const {csvParse} = dsv;
const {promisify} = util;

const GIRONDE_DEPARTEMENT_CODE = '33'
const FOCUSED_DEPARTEMENT_CODE = GIRONDE_DEPARTEMENT_CODE;

const INPUT_COMMUNE_DATA_FILE_PATH = 'public/data/table-appartenance-geo-communes-15.csv';
const INPUT_MIGRATION_FILE_PATH = 'data/RP2015_MIGCOM_dbase/FD_MIGCOM_2015.dbf';
const INPUT_COMMUNE_EPCI_SCOP_FILE_PATH = 'public/data/tableau_correspondances.csv';

const OUTPUT_COMMUNE_MIGRATION_FILE_PATH = 'public/data/migration-communes-girondes.json';
const OUTPUT_EPCI_MIGRATION_FILE_PATH = 'public/data/migration-EPCI-girondes.json';
const OUTPUT_SCOT_MIGRATION_FILE_PATH = 'public/data/migration-SCoT-girondes.json';

const readFile = promisify(fs.readFile)

const correspP = readFile(INPUT_COMMUNE_DATA_FILE_PATH, 'utf-8')
.then(csvParse)

const communeByCodeP = correspP
.then(communes => {
    const communeByCode = new Map()
    for(const commune of communes){
        communeByCode.set(commune['Code géographique'], commune)
    }
    return communeByCode;
});


const epciScotP = readFile(INPUT_COMMUNE_EPCI_SCOP_FILE_PATH, 'utf-8')
.then(csvParse)

const EPCIByCommuneCodeP = epciScotP
.then(correspondances => {
    const EPCIByCommuneCode = new Map()
    for(const corresp of correspondances){
        EPCIByCommuneCode.set(corresp['CODE_COMMUNE'], corresp['CODE_EPCI'])
    }
    return EPCIByCommuneCode;
})

const SCoTByCommuneCodeP = epciScotP
.then(correspondances => {
    const SCoTByCommuneCode = new Map()
    for(const corresp of correspondances){
        SCoTByCommuneCode.set(corresp['CODE_COMMUNE'], corresp['CODE_SCOT'])
    }
    return SCoTByCommuneCode;
})


const parser = new Parser(INPUT_MIGRATION_FILE_PATH);



{ // parsing lifecycle
    parser.on('start', () => {
        console.log('dBase file parsing has started');
    });
    
    parser.on('header', (h) => {
        console.log('dBase file header has been parsed', h);
    });

    let recordCount = 0;

    parser.on('record', (record) => {
        if(recordCount === 0)
            console.log(record)

        recordCount++
        if(recordCount % 50000 === 0)
            console.log(recordCount)
    })

    console.time('parse')

    parser.on('end', (p) => {
        console.log('Finished parsing the dBase file', recordCount);
        console.timeEnd('parse')
    })
}


function accumulateMigrationRecord(map, originCode, destinationCode, record){
    let destinationInfos = map.get(destinationCode)
    if(!destinationInfos){
        destinationInfos = {
            "destinationCode": destinationCode,
            "migrationsByOriginCode": Object.create(null)
        }
    }

    // add migration
    const {migrationsByOriginCode} = destinationInfos;
    let migrationOrigins = migrationsByOriginCode[originCode] || [];

    const {AGEREVQ, CS1, NPERR, TACT, IPONDI} = record

    const relevantMigration = migrationOrigins.find((migr) => {
        return ['AGEREVQ', 'CS1', 'NPERR', 'TACT'].every(field => migr[field] === record[field])
    })

    if(relevantMigration){
        relevantMigration['IPONDI'] += IPONDI
    }
    else{
        migrationOrigins.push({AGEREVQ, CS1, NPERR, TACT, IPONDI});
    }

    migrationsByOriginCode[originCode] = migrationOrigins;
    destinationInfos.migrationsByOriginCode = migrationsByOriginCode;

    map.set(destinationCode, destinationInfos)
}



Promise.all([communeByCodeP, EPCIByCommuneCodeP, SCoTByCommuneCodeP])
.then( ([communeByCode, EPCIByCommuneCode, SCoTByCommuneCode]) => {
    console.log('communeByCode, EPCIByCommuneCode, SCoTByCommuneCode are ready')

    const girondeCommuneMigrationsByDestinationCommuneCode = new Map();
    const girondeEPCIMigrationsByDestinationEPCICode = new Map();
    const girondeSCoTMigrationsByDestinationSCoTCode = new Map();

    parser.on('record', (record) => {
        let communeCode = record['COMMUNE'];
        let originCode = record['DCRAN'];

        // communeByCode.get(code) can be undefined if code refers to foreign countries
        const currentCommune = communeByCode.get(communeCode) || {};
        const previousCommune = communeByCode.get(originCode) || {};

        if (communeCode === originCode) {
          return;
        }

        if(currentCommune['Département'] !== FOCUSED_DEPARTEMENT_CODE &&
            previousCommune['Département'] !== FOCUSED_DEPARTEMENT_CODE
        ){
            return;
        }

        if(previousCommune['Département'] !== FOCUSED_DEPARTEMENT_CODE){
            const regionCode = previousCommune['Région 2016'];
            if(!regionCode){ // foreign country
                // TODO decide what to do better than ignore
                return;
            }

            // transform commune code to a region-based code to accumulate all coming from the same region
            originCode = 'R'+regionCode;
        }

        if(currentCommune['Département'] !== FOCUSED_DEPARTEMENT_CODE){
            const regionCode = currentCommune['Région 2016'];
            if(!regionCode){ // foreign country
                // TODO decide what to do better than ignore
                return;
            }

            // transform commune code to a region-based code to accumulate all coming from the same region
            communeCode = 'R'+regionCode;
        }

        accumulateMigrationRecord(girondeCommuneMigrationsByDestinationCommuneCode, originCode, communeCode, record)


        // EPCI
        let communeEPCICode = communeCode.startsWith('R') ? communeCode : EPCIByCommuneCode.get(communeCode)
        let originEPCICode = originCode.startsWith('R') ? originCode : EPCIByCommuneCode.get(originCode)
        
        if(communeEPCICode && originEPCICode && communeEPCICode !== originEPCICode){
            accumulateMigrationRecord(girondeEPCIMigrationsByDestinationEPCICode, originEPCICode, communeEPCICode, record)
        }

        // SCoT
        let communeSCoTCode = communeCode.startsWith('R') ? communeCode : SCoTByCommuneCode.get(communeCode)
        let originSCoTCode = originCode.startsWith('R') ? originCode : SCoTByCommuneCode.get(originCode)
        
        if(communeSCoTCode && originSCoTCode && communeSCoTCode !== originSCoTCode){
            accumulateMigrationRecord(girondeSCoTMigrationsByDestinationSCoTCode, originSCoTCode, communeSCoTCode, record)
        }

    });

    parser.on('end', (p) => {

        fs.writeFile(
            OUTPUT_COMMUNE_MIGRATION_FILE_PATH, 
            JSON.stringify([...girondeCommuneMigrationsByDestinationCommuneCode.values()], null, 2), 'utf8', 
            err => err && console.error(`error writing ${OUTPUT_COMMUNE_MIGRATION_FILE_PATH}`, err)
        )
        fs.writeFile(
            OUTPUT_EPCI_MIGRATION_FILE_PATH, 
            JSON.stringify([...girondeEPCIMigrationsByDestinationEPCICode.values()], null, 2), 'utf8', 
            err => err && console.error(`error writing ${OUTPUT_EPCI_MIGRATION_FILE_PATH}`, err)
        )
        fs.writeFile(
            OUTPUT_SCOT_MIGRATION_FILE_PATH, 
            JSON.stringify([...girondeSCoTMigrationsByDestinationSCoTCode.values()], null, 2), 'utf8', 
            err => err && console.error(`error writing ${OUTPUT_SCOT_MIGRATION_FILE_PATH}`, err)
        )
    });

    parser.parse();

})

process.on('uncaughtException', (err) => {
    console.error('uncaught', err)
})
